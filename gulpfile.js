const gulp          = require("gulp");
const sass          = require("gulp-sass");
const plumber       = require("gulp-plumber");
const postcss       = require("gulp-postcss");
const autoprefixer  = require("autoprefixer");
const minify        = require("gulp-csso");
const gcmq          = require('gulp-group-css-media-queries');
const svgstore      = require("gulp-svgstore");
const rename        = require("gulp-rename");
const replace       = require("gulp-replace");
const posthtml      = require("gulp-posthtml");
const include       = require("posthtml-include");
const includer      = require("gulp-x-includer");
const pug           = require('gulp-pug');
const cheerio       = require('gulp-cheerio');
const svgmin        = require('gulp-svgmin');
const svgSprite     = require('gulp-svg-sprite');
const htmlbeautify  = require('gulp-html-beautify');
const server        = require("browser-sync").create();


function style() {
    return gulp.src("source/static/sass/style.scss")
        .pipe(plumber())
        .pipe(sass({
            includePaths: require('node-normalize-scss').includePaths
        }))
        .pipe(postcss([
            autoprefixer()
        ]))
        .pipe(gcmq())
        .pipe(gulp.dest("source/static/css"))
        .pipe(server.stream());
}

function sprite() {
    return gulp.src("source/static/images/*.svg")
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        .pipe(cheerio({
            run: $ => {
                $('[fill]').removeAttr('fill');
                $('[stroke]').removeAttr('stroke');
                $('[style]').removeAttr('style');
            },
            parserOptions: { xmlMode: true }
        }))
        .pipe(replace('&gt;', '>'))
        .pipe(svgSprite({
            mode: {
                symbol: {
                    sprite: 'sprite.svg'
                }
            }
        }))
        .pipe(gulp.dest("source/static/images"));
}

function htmlbeautifyTask() {
    return gulp.src('source/*.html')
        .pipe(htmlbeautify())
        .pipe(gulp.dest('source'))
}

function views() {
    return gulp.src('source/pug/*.pug')
        .pipe(pug({
            pretty: true
        }))
        .pipe(htmlbeautify())
        .pipe(gulp.dest("source"))
        .on('end', server.reload);
}

function html() {
    return gulp.src("source/*.html")
        .pipe(posthtml([
            include()
        ]))
        .pipe(gulp.dest("source"));
}

function includeTask() {
    return gulp.src(["source/parts/*.html"])
        .pipe(includer())
        .pipe(gulp.dest("source/"));
}

function serve() {
    server.init({
        server: "source/",
        notify: false,
        port: "2222",
        open: true,
        cors: true,
        ui: false
    });
    gulp.watch("source/static/sass/**/*.{scss,sass}", gulp.series(style));
    gulp.watch("source/pug/**/*.pug", gulp.series(views));
    gulp.watch("source/pug/**/*.pug").on("change", server.reload);
    gulp.watch("source/static/js/*.js").on("change", server.reload);
}


module.exports.style            = style;
module.exports.sprite           = sprite;
module.exports.views            = views;
module.exports.htmlbeautifyTask = htmlbeautifyTask;
module.exports.html             = html;
module.exports.includeTask      = includeTask;
module.exports.serve            = gulp.series(sprite, style, views, html, htmlbeautifyTask, serve);