$(document).ready(function() {
	$(window).on('load', function () {
		AOS.refresh();
	});
	$(function () {
		AOS.init();
	});
	$(document).on('scroll', () => {
		if ($(document).scrollTop() > 100) $('.header').addClass('header_small');
		else $('.header').removeClass('header_small');
		if ($(document).scrollTop() > 400) {
			$('.catalog-tabs__pane.active .scroll-btn').show()
		}
		else $('.catalog-tabs__pane.active .scroll-btn').hide();
		if($(document).scrollTop() >= $('.catalog-tabs__pane.active').height() - 400)
			$('.catalog-tabs__pane.active .scroll-btn').hide();
		
	});
	$(window).on('resize', () => {
		if ($(document).width() > 1280) {
			$('.header-collapse').hide();
		}
	});
	$('#modal-apply').on('hide.bs.modal', () => {
		$('#apply-form')[0].reset();
		$('.modal-apply-form__files').empty();
		$('.modal-apply-form__files').css({ 'display': 'none'});
		$('.modal-apply-form__label.top > .modal-apply-form__span').css({ 'display': 'block'});
		$('.modal-apply-form__upload').css({ 'width': 'auto'});
	});
	$('#modal-callback').on('hide.bs.modal', () => {
		$('#callback-form')[0].reset();
	});
	$('#modal-calc').on('hide.bs.modal', () => {
		$('#calc-form')[0].reset();
	});
	
	$select = $('.modal-calc-form__select');
	$.each($select, (i, val) => {
		if ($(val).val() == null) $(val).css({'color': '#A3A4A7'});
	});
	$('.phone-mask').inputmask("+7 (999) 999-99-99");
	$('.form-result-ok__btn').on('click', () => {
		$('#modal-form-ok').hide();
	});
	$('.form-result-not-ok__btn').on('click', () => {
		$('#modal-form-not-ok').hide();
	});
	$('#modal-searchbar').on('hide.bs.modal', () => {
		$('#searchbar-form')[0].reset();
		$('#modal-searchbar .modal-searchbar-results').css({'display': 'none'});
	});
	$('#modal-searchbar .modal-searchbar-form__btn').on('click', (e) => {
		e.preventDefault();
		$('#modal-searchbar .modal-searchbar-results').css({'display': 'block'});
	});
	$('.modal-apply-form__upload .modal-apply-form__btn').on('click', (e) => {
		$('.modal-apply-form__files').css({ 'display': 'block'});
		$(`
			<div class="modal-apply-form__file">
				<input class="modal-apply-form__input" type="file" name="upload[]">
			</div>
		`).appendTo($('.modal-apply-form__files'))
		.children('input').click()
		.on('change', function (e) {
			$('.modal-apply-form__label.top > .modal-apply-form__span').css({ 'display': 'none'});
			$('.modal-apply-form__upload').css({ 'width': '100%'});
			$(`
				<div class="modal-apply-form__file-wrap">
					<span class="modal-apply-form__file-name"> ${e.target.files[0].name} </span>
					<i class="icon icon-cancel"></i>
				</div>
			`).appendTo($(this).parent())
			.on('click', function(e) {
				if ($(e.target).is('.icon-cancel') ) {
					e.preventDefault();
					$(this).parent().remove();
					if ($('.modal-apply-form__files').children().children('.modal-apply-form__file-wrap').length == 0) {
						$('.modal-apply-form__files').css({ 'display': 'none'});
						$('.modal-apply-form__label.top > .modal-apply-form__span').css({ 'display': 'block'});
						$('.modal-apply-form__upload').css({ 'width': 'auto'});
					}
				}
			});
		});
	});
	$('.header .header-btn').on('click', (e) => {
		const menuBtn = $(e.target).parent('button');
		$('.header').removeClass('header_small');
	});
	$('.header-burger').on('click', () => {
		$('.header-collapse').slideToggle();
		$('.header-burger__open').toggle();
		$('.header-burger__close').toggle();
	});
	$('.header-wrap .header-categories__item').on('click', (e) => {
		const categoriesItem = $(e.target).parent('li');
		categoriesItem.children('.header-categories__submenu').show();
		if ($(e.target).is('a') && !categoriesItem.hasClass('header-categories__item_active')) {
			$('.header-categories__item').children('.header-categories__submenu').hide();
			$('.header-categories__item').removeClass('header-categories__item_active');
			categoriesItem.addClass('header-categories__item_active');
			categoriesItem.children('.header-categories__submenu').show();
		} else {
		};
	});
	$('.header-wrap .header-categories__submenu .close').on('click', (e) => {
		$(e.target).closest('.header-categories__submenu').hide();
	});
	$('.header-collapse .header-categories__item').on('click', (e) => {
		const categoriesItem = $(e.target).parent('li');
		categoriesItem.children('.header-categories__submenu').slideToggle();
		categoriesItem.children('.icon-dropdown').toggleClass('icon-dropdown_up');
		if ($(e.target).is('a') && !categoriesItem.hasClass('header-categories__item_active')) {
			$('.header-categories__item').children('.header-categories__submenu').hide();
			$('.header-categories__item').children('.icon-dropdown').removeClass('icon-dropdown_up');
			$('.header-categories__item').removeClass('header-categories__item_active');
			categoriesItem.addClass('header-categories__item_active');
			categoriesItem.children('.header-categories__submenu').slideDown();
			categoriesItem.children('.icon-dropdown').addClass('icon-dropdown_up');
		} else {
		};
	});
	$('.main-popular .slick-mobile-arrows .slick-prev').on('click', (e) => {
		const slickPrev = $(e.target);
		slickPrev.parent().parent().find('.main-popular-wrap').slick('slickPrev');
	});
	$('.main-popular .slick-mobile-arrows .slick-next').on('click', (e) => {
		const slickNext = $(e.target);
		slickNext.parent().parent().find('.main-popular-wrap').slick('slickNext');
	});
	$('.main-popular-wrap').slick({
		mobileFirst: true,
		rows: 1,
		slidesPerRow: 1,
		dots: false,
		adaptiveHeight: true,
		responsive: [
			{
				breakpoint: 568,
				settings: {
					rows: 2,
					slidesPerRow: 2,
					dots: true,
					arrows: true
				}
			},
			{
				breakpoint: 768,
				settings: {
					rows: 2,
					slidesPerRow: 2,
					dots: true,
					arrows: true
				}
			},
			{
			  breakpoint: 992,
			  settings: {
				  rows: 2,
				  slidesPerRow: 3,
				  dots: true,
				  arrows: true
			  }
			}
		]
	});
	$('.about-staff-content').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		dots: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					dots: false,
				}
			},
			{
				breakpoint: 380,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: false,
					arrows: false
				}
			},
		]
	});
	$('.product-card__active').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.product-card__images'
	});
	$('.product-card__images').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		asNavFor: '.product-card__active',
		arrows: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				}
			},
			{
				breakpoint: 568,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4,
				}
			}
		]
	});
	$('.product-card__details-counter-input').on('change', (e) => {
		if ($(e.target).val() == '') $(e.target).val('1');
	});
	$('.news-single-photo').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		draggable: false,
		arrows: false,
		fade: true,
		asNavFor: '.news-single-miniatures'
	});
	$('.news-single-miniatures').slick({
		slidesToShow: 5,
		slidesToScroll: 5,
		vertical: true,
		draggable: false,
		asNavFor: '.news-single-photo',
		arrows: false,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					vertical: false,
					draggable: true,
				}
			},
			{
				breakpoint: 768,
				settings: {
					vertical: false,
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 568,
				settings: {
					vertical: false,
					slidesToShow: 5,
					slidesToScroll: 5,
				}
			},
		]

	});
	$('#order-form').on('submit', function (e) {
		e.preventDefault();
        const formData = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: 'submit.php',
            data: formData,
            success: function() {
                $('#order-form')[0].reset();
                $('.product-card__details-btn .icon').css({'display': 'block'});
				$('.product-card__details-btn').val('добавлено');
            }
		});
    });
	$('.product-featured__list').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		dots: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					dots: false
				}
			},
			{
				breakpoint: 380,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false
				}
			},
		]
	});
	$('.order-item__delete').on('click', function(e) {
		e.preventDefault();
		$(this).parent().parent().remove();
	});
});

// multistep calc form

let currentTab = 0;
$tabs = $('.modal-calc-form__tab');
$($tabs).css({'display': 'none'});
showTab(currentTab);

function showTab(n) {
	$('.modal-calc-form__step').html(`Шаг ${n + 1} из ${$tabs.length}`);
	$($tabs[n]).css({'display': 'block'});
	if (n == 0)	$('#calc-form-prev').css({'display': 'none'});
	else $('#calc-form-prev').css({'display': 'block'});
	if (n == $tabs.length - 1) {
		$('.modal-calc-form__step').html(`Шаг ${n + 1} из ${$tabs.length} - Результаты расчета`);
		$('#calc-form-next').html('Отправить заявку');
	}
	else ($('#calc-form-next').html('Далее'));
}

function prevStep() {
	$($tabs[currentTab]).css({'display': 'none'});
	currentTab--;
	console.log(currentTab);
	if (currentTab == $tabs.length - 1) {
		$('#calc-form').submit();
		return false;
	}
	showTab(currentTab);
}

function nextStep() {
	$($tabs[currentTab]).css({'display': 'none'});
	currentTab++;
	console.log(currentTab);
	if (currentTab >= $tabs.length) {
		$('#calc-form').submit();
		return false;
	}
	showTab(currentTab);
}

function validateForm() {

}

//  google map

function initMap() {
	const myLatlng = new google.maps.LatLng(55.75589, 37.7630);
	const mapOptions = {
		zoom: 14,
		center: { lat: 55.7448, lng: 37.7475 },
		mapTypeId: 'roadmap',
		disableDefaultUI: true,
		styles: [
			{
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#212121"
				}
				]
			},
			{
				"elementType": "labels.icon",
				"stylers": [
				{
					"visibility": "off"
				}
				]
			},
			{
				"elementType": "labels.text.fill",
				"stylers": [
				{
					"color": "#757575"
				}
				]
			},
			{
				"elementType": "labels.text.stroke",
				"stylers": [
				{
					"color": "#212121"
				}
				]
			},
			{
				"featureType": "administrative",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#757575"
				}
				]
			},
			{
				"featureType": "administrative.country",
				"elementType": "labels.text.fill",
				"stylers": [
				{
					"color": "#9e9e9e"
				}
				]
			},
			{
				"featureType": "administrative.land_parcel",
				"stylers": [
				{
					"visibility": "off"
				}
				]
			},
			{
				"featureType": "administrative.locality",
				"elementType": "labels.text.fill",
				"stylers": [
				{
					"color": "#bdbdbd"
				}
				]
			},
			{
				"featureType": "poi",
				"elementType": "labels.text.fill",
				"stylers": [
				{
					"color": "#757575"
				}
				]
			},
			{
				"featureType": "poi.park",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#181818"
				}
				]
			},
			{
				"featureType": "poi.park",
				"elementType": "labels.text.fill",
				"stylers": [
				{
					"color": "#616161"
				}
				]
			},
			{
				"featureType": "poi.park",
				"elementType": "labels.text.stroke",
				"stylers": [
				{
					"color": "#1b1b1b"
				}
				]
			},
			{
				"featureType": "road",
				"elementType": "geometry.fill",
				"stylers": [
				{
					"color": "#2c2c2c"
				}
				]
			},
			{
				"featureType": "road",
				"elementType": "labels.text.fill",
				"stylers": [
				{
					"color": "#8a8a8a"
				}
				]
			},
			{
				"featureType": "road.arterial",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#373737"
				}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#3c3c3c"
				}
				]
			},
			{
				"featureType": "road.highway.controlled_access",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#4e4e4e"
				}
				]
			},
			{
				"featureType": "road.local",
				"elementType": "labels.text.fill",
				"stylers": [
				{
					"color": "#616161"
				}
				]
			},
			{
				"featureType": "transit",
				"elementType": "labels.text.fill",
				"stylers": [
				{
					"color": "#757575"
				}
				]
			},
			{
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [
				{
					"color": "#000000"
				}
				]
			},
			{
				"featureType": "water",
				"elementType": "labels.text.fill",
				"stylers": [
				{
					"color": "#3d3d3d"
				}
				]
			}
		]
	};
	const map = new google.maps.Map(document.getElementById('map'), mapOptions);
	const icon =  {
		url: 'static/images/map-pin.svg',
		size: new google.maps.Size(82, 82),
		anchor: new google.maps.Point(41, 41)
	};
	const marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: icon
	});
	if ($(document).width() < 991) {
		map.setCenter({ lat: 55.7574, lng: 37.7533 });
	} else {
		map.setCenter({ lat: 55.7448, lng: 37.7475 });
	}
	google.maps.event.addDomListener(window, "resize", function() {
		google.maps.event.trigger(map, "resize");
		if ($(document).width() < 991) {
			map.setCenter({ lat: 55.7574, lng: 37.7533 });
		} else {
			map.setCenter({ lat: 55.7448, lng: 37.7475 });
		}
		
	});
}

//  product count

function increaseCount(el) {
	const input = el.nextElementSibling;
	let value = parseInt(input.value, 10);
	value = isNaN(value) ? 0 : value;
	value++;
	input.value = value;
}

function decreaseCount(el) {
	const input = el.previousElementSibling;
	let value = parseInt(input.value, 10);
	if (value > 1) {
		value = isNaN(value) ? 0 : value;
		value--;
		input.value = value;
	}
}
